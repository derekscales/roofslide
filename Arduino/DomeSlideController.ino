const int REMOTE_PIN = 12;

int RoofPosition = 0; // 0 is closed, 1 is open

void setup()
{
	// Set up the input and output pins
	pinMode(REMOTE_PIN, OUTPUT);

	// set up the serial port
	Serial.begin(57600);
	Serial.flush();
}

void loop()
{
	String cmd;

	if (Serial.available() > 0) {
		cmd = Serial.readStringUntil('#');
		if (cmd == "GETPOSITION") {
			Serial.print(RoofPosition);
			Serial.println("#");
		}
		else if (cmd == "CLOSEROOF")
		{
			if (RoofPosition == 1)
			{
				// Roof is open so now we can close the roof
				CloseRoof();
			}
			else
			{
				// Error roof closed or moving so don't change anything (if we are opening/closing we will continue to open or close)
				Serial.println("1#");
			}
		}
		else if (cmd == "OPENROOF")
		{
			if (RoofPosition == 0)
			{
				OpenRoof();
			}
			else
			{
				// Error roof open or moving so don't change anything (if we are opening/closing we will continue to open or close)
				Serial.println("1#");
			}
		}
	}
}

void CloseRoof()
{
	// Press button
	PressButton();
	// Note the roof as closed
	RoofPosition = 0;
	Serial.println("0#");
}

void OpenRoof()
{
	// Press button
	PressButton();
	// Note the roof as open
	RoofPosition = 1;
	Serial.println("0#");
}

void PressButton()
{
	// Set the pin high
	digitalWrite(REMOTE_PIN, HIGH);
	// Wait 100 ms
	delay(500);
	// Set the pin low
	digitalWrite(REMOTE_PIN, LOW);
	delay(250);
}
